/* Javascript de compatibilidade para o IE8 */

/*==============================================
=            Declaração das funções            =
==============================================*/


/*==========  Definir Menu Ativo  ==========*/

function fncSVG() {
	$('img[src$=".svg"]').each(function() {
        $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });
}
/*-----  End of Declaração das funções  ------*/


/*===========================================
=            Chamada das funções            =
===========================================*/

if (Modernizr.svg) {
	fncSVG();
}

/*-----  End of Chamada das funções  ------*/