/*!
 * Gruntfile [NOME DO PROJETO]
 * http://www.e-construmarket.com.br
 * @author Roger Ramos
 */
module.exports = function( grunt ) {
 'use strict';
  grunt.initConfig({

    // Limpar saída
    clean: {
      dist: ['www'],
      css: ['www/_assets/css'],
      js: ['www/_assets/js'],
      html: ['www/*.html'],
      img: ['www/_assets/img/**/*']
    },

    //Connect live reload
    connect: {
      server: {
        options: {
          protocol: 'http',
          port: 8181,
          base:'www/',
          livereload: true,
          keepalive: true
        }
      }
    },

    //Compass
    compass: {
      dist: {
        options: {
          config: 'config.rb',
          trace: true
        }
      }
    },

    copy: {
      js: {
        files: [
          {expand: true, cwd: '_dev/_assets/js/', src: ['**'], dest: 'www/_assets/js/'}
        ]
      },
      html: {
        files: [
          {expand: true, cwd: '_dev/', src: ['*.html'], dest: 'www/'}
        ]
      },
      img: {
        files: [
          {expand: true, cwd: '_dev/_assets/img/', src: ['**/*.png', '**/*.gif'], dest: 'www/_assets/img/'},
          ]
       }
    },

    //Minificação das imagens
    imagemin: {
      dynamic: { 
        options: {
            optimizationLevel: 3
        },
        files: [{
          expand: true,
          cwd: '_dev/_assets/img',
          src: ['**/*.{jpg,gif}'],
          dest: 'www/_assets/img'
        }]
      }
    },

    //include
    includes: {
      build: {
        cwd: '_dev',
        src: [ '*.html' ],
        dest: 'www/',
        options: {
          flatten: true,
          includePath: '_dev/includes',
          banner: '<!-- Site built using grunt includes! -->\n'
        }
      }
    },

    //JShint
    jshint: {
      all: [
        '_dev/**/*.js'
      ],
      options: {
        jshintrc: '.jshintconfig'
      }
    },

  	//Uglify
  	uglify: {
  		options: {
  			mangle:false
  		},
  		my_target: {
  			files: {
  				'_assets/js/html5shiv.js' : ['_dev/js/html5shiv.js'],
  				'_assets/js/scripts.js' : ['_dev/js/jquery.js']
  			}
  		}
  	},

  	//Watch
  	watch: {
      css: {
  			files: ['_dev/_assets/scss/**', '_dev/_assets/scss/*.scss'],
  			tasks: ['clean:css', 'compass'],
        options: {
          livereload: true
        }
  		},
      js: {
        files: ['_dev/_assets/js/**', '_dev/_assets/js/*.js'],
        tasks: ['clean:js', 'copy:js'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['_dev/*.html', '_dev/**/*.html'],
        tasks: ['clean:html', 'includes'],
        options: {
          livereload: true
        }
      },
      img: {
        files: ['_dev/_assets/img/**', '_dev/_assets/img/*'],
        tasks: ['clean:img', 'imagemin', 'clean:sprites'],
        options: {
          livereload: true
        }
      }
  	}

  });

  //Plugins do Grunt
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  //Tarefas Grunt
  grunt.registerTask( 'build', ['clean','includes', 'compass', 'copy:js', 'copy:img', 'imagemin'] );
  grunt.registerTask( 'default', ['build','jshint', 'watch'] );

};