# Require any additional compass plugins here.

# Removing all comments by applying a monkey patch to SASS compiler
#require "./remove-all-comments-monkey-patch"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "www/_assets/css"
sass_dir = "_dev/_assets/scss"
images_dir = "_dev/_assets/img"
generated_images_dir = "www/_assets/img"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass