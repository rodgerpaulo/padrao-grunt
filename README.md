# INSTALAÇÃO #

Para configurar o padrão é fácil, basta seguir os passos abaixo:

**1** - Clone este repositório:
```
#!Git

git clone git@bitbucket.org:rodgerpaulo/padrao-grunt.git
```

**2** - Quando você clonar este repositório, ele também vai trazer as configurações remotas, logo será necessário remover antes de começar a trabalhar:
```
#!Git

git remote rm origin
```


**3** - Agora adicione a url do repositório que você criou:

```
#!Git

git remote add origin <url>
```


**4** - Agora você precisa digitar o comando abaixo para informar ao *Git* que o repositório remoto vai atualizar o *Branch* atual, Presumindo que você esteja no *Branch Master*

```
#!Git

git push --set-upstream origin master

```

**5** - Só dar um *push* para subir adicionar os arquivos ao repositório que você criou:

```
#!Git

git push

```

Have fun!